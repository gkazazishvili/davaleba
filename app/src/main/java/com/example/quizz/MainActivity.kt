package com.example.quizz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        var g = listOf<Int>(5,3,2,6,6,3,9)
        var test = listOf<Int>(1,3,4,5,6,7,7,6,5,9,12,15)
        var g2 = listOf<Int>(8,6,4,9,5,3)


        var diffElAr = diffElementsInArray(g)
        var tankvtAr = tanakveta(g,g2)
        var mrgAr = merge(g,g2)
        var sashArtArr = sashAritmNaklebi(test)
        var secMinMax = secondMinAndMax(test)

        d("davaleba_diffEl", "განსხვავებული ელემენტი: $diffElAr")
        d("davaleba_tanakveta", "საერთო ელემენტებია: $tankvtAr")
        d("davaleba_merge", "გაერთიანებული მასივი:  $mrgAr")
        d("davaleba_sashAritmNakl", "განსხვავებული ელემენტი: $sashArtArr")
        d("davaleba_secondMinMax", "მინ მეორე - მაქს მეორე: $secMinMax")

    }

    fun diffElementsInArray(intArray: List<Int>):Int{
        var clonedArr = intArray
        //ვხმარობ სეტ-ს უნიკალური ელემენტებისთვის
        var uniqueElems = clonedArr.toSet()

        var count = 0
        for(i in uniqueElems.indices){
            //count-ით ვითვლი რამდენი უნიკალური ელემენტია
            count++
        }
        return count
    }

    fun tanakveta(intArray: List<Int>, intArray2: List<Int>): MutableSet<Int> {

        var firstArr = intArray
        var secondArr = intArray2

        var arrayWithTanakvetaElements = mutableSetOf<Int>()

        //ვიღებ უნიკალურ ელემენტებს სეტ-ით და ვამოწმებ პირველი და მეორე მასივის თანაკვეთას, ვამატებ მესამე მასივში
        for(i in secondArr.indices){
            if(firstArr.contains(secondArr.get(i))) {
                arrayWithTanakvetaElements.add(secondArr.get(i))
            }
        }
        return arrayWithTanakvetaElements
    }

    fun merge(intArray: List<Int>, intArray2: List<Int>): MutableList<Int> {

        var arrayWithMergeElements = mutableListOf<Int>()

        arrayWithMergeElements.addAll(intArray)
        arrayWithMergeElements.addAll(intArray2)

        return arrayWithMergeElements

    }

    fun sashAritmNaklebi(intArray: List<Int>): MutableList<Int> {
        var clonedArr = intArray
        var lessThanArray = mutableListOf<Int>()
        var sum = 0
        for (i in clonedArr.indices){
            sum += clonedArr[i]
        }
        //საშუალო არითმეტიკული
        sum /= clonedArr.size

        for(i in clonedArr.indices){
            //ვამოწმებ არის თუ არა ელემენტების საშუალო არითმეტიკულზე ნაკლები და ვამატებ ცარიელ lessThanArray-ში
            if(sum > clonedArr.get(i)){
                lessThanArray.add(clonedArr.get(i))
            }
        }
        return lessThanArray
    }


    fun secondMinAndMax(intArray: List<Int>):MutableList<Int>{
        var clonedArr = intArray
        clonedArr = clonedArr.sorted()
        var minMax = mutableSetOf<Int>()
        minMax = clonedArr.toSet() as MutableSet<Int>
        var backToArray = minMax.toIntArray()
        var finalArr = arrayOf(backToArray[1], backToArray[backToArray.size-2])
        return finalArr.toMutableList()
    }

}